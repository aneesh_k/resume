ANEESH KULKARNI
Boston, MA      kulkarni.ane@husky.neu.edu      www.linkedin.com/in/kulkarniane

Education
Northeastern University, Boston, MA							         	          	   Expected December 2019
Master of Science in Computer Systems Networking and Telecommunication, GPA 3.82
Relevant Courses: Network Security, Connected Devices (IoT), Data Networking, Linux for Network Engineers, Telecommunication and Network Infrastructure, Internet Protocols and Architecture.

University of Pune, Pune, MH, India				    	             						May 2016
Bachelor of Engineering in Electronics and Telecommunication, Distinction.

Technical Skills
Programming languages | OS:  C, Python, Bash  |  Windows, Linux/Unix (Debian), MacOS.
Protocols:  DNS, DHCP, HTTP, TCP/IP, UDP, NAT, BGP, OSPF, MPLS, HSRP, VLAN (802.1Q), SSL/TLS, RADIUS, RTP, VoIP, STUN/TURN, MQTT, CoAP.
Tools/Frameworks:  AWS-VPC, Cisco Packet Tracer, Wireshark, Multisim, VMWare Workstation/VSphere, Putty, MobaXterm, Postman, Docker, JSON, XML, Selenium, JIRA, GIT.
Microcontroller Systems:  PIC 18F, Arduino, Raspberry Pi, Beaglebone.

Work Experience
Systems Engineer, Network (Co-op), Benu Networks, Billerica, MA.			            	           August 2018-December 2018
-	Network Setup: Configured WAGs (Wireless Area Gateways) to allow subscriber sessions using Benu’s platform. Configured switches, access points, GRE/L2TPV3 tunnels to monitor traffic for SP-WiFi, ‘Managed Home Network’ (Internet + IoT solution), ‘Managed Business Network’.
-	Testing and Tracking: Dynamically logged, tracked and monitored software bugs on JIRA; worked with other developers to identify and fix redundancies. Had responsibility of developing test cases, comprehensive testing and validation for ‘MHN’ product and new wireless access points before shipping.
-	Feature Development: Part of a five-member team to develop a network monitoring framework using synthetic clients to check real-time network performance and statistics. The framework was worth millions of dollars.
-	Automation: Developed an automation framework from scratch using python and selenium for automated testing of traffic and user-flows. The project is part of a POC worth millions of dollars for a large internet provider in the US.

Research Assistant, Genesis Lab, Northeastern University, Boston, MA.		 January 2019-June 2019; May 2018-December 2018
-	Connected Devices Setup: Part of a wireless charging product team where I developed the systems’ backend from ground up: comprising of a HTTP server to live-stream ambient environment heat map, an IoT server and client architecture, DNS and DHCP servers, ad-hoc WiFi access point and actuation circuits for control of devices. The project won third place in a demo-pitch competition at an NSF conference.
-	Software Development: Developed a highly specialized system in python for a software-defined wireless power transfer framework: to manage device charging, log charging sessions and report data to authorized devices for phones/laptops and drones.
-	Wireless Power Transfer Research: Part of a three-member team working on novel methods for wireless power transfer. Worked on interfacing hardware (Coils) with embedded microcontrollers, creating a novel magnetic resonance sensing mechanism, back-end network and software infrastructure. 

Industrial Projects
Automatic Bi-directional Count Tracking Machine		 	            		   	  	     August 2015-April 2016
-	Built an embedded resistor tracking machine for ‘Vishay Components India Pvt. Ltd.’ to be integrated with the manufacturing assembly line. Added several features to make it more operator friendly: for example, adding buzzer for alerts, pre-setting the resistor count.
-	Increased efficiency by reducing delay caused by manually counting resistors; automated some of the system.

Academic Projects
Network Security					  	          	          	                	              January 2019-August 2019
-	Coded an Intrusion Detection script with open source tools to detect teardrop attacks and TCP re-fragment attacks trying to bypass IDS.
-	Wrote a script to identify BGP anomalies in BGP exchange messages.
-	Leveraged open source libraries for DNSSEC and DNSCrypt to for formulate a DNS stub resolver to check for fraudulent DNS replies.

Home Automation IoT infrastructure									   January 2018-April 2018
-	Built an embedded home automation system using MQTT and CoAP libraries for controlling/actuating switches.

Virtualized DHCP Server, DNS Server, Firewall, and Backup			               	   	  	    November 2017
-	Designed a LAN setup using VMware workstation to simulate a base network infrastructure.
